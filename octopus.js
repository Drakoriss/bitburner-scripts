import { PORT_EMPTY, PORT_ROOTKIT, PORT_TENTACLE, PORT_TENTACLE_FEEDBACK, HOST_HOME, SCRIPT_OCTOPUS, SCRIPT_RETRACT, SCRIPT_ROOT_KIT, SCRIPT_TENTACLE, SCRIPTS_RESERVE } from './common.js';
import { loop, log, getAllHosts, withRootAccess, clamp, logBase } from './utils.js';

const BALANCE_FACTOR = 240;
const BIT_NODE_HACK_MONEY = {
    3: 0.2,
    4: 0.2,
    5: 0.15,
    6: 0.75,
    7: 0.5,
    8: 0.3,
    9: 0.1,
    10: 0.5,
    13: 0.2,
};
const GROWTH_RATE = 1.03;
const GROWTH_RATE_MAX = 1.0035;
const BIT_NODE_GROW = {
    2: 0.8,
    3: 0.2,
    11: 0.2,
};
let SECURITY_PROGRESS;

/** @param {NS} ns **/
export async function main(ns) {
    BIT_NODE_HACK_MONEY['12'] = setBitNode12Mult(ns);
    BIT_NODE_GROW['12'] = BIT_NODE_HACK_MONEY['12'];
    SECURITY_PROGRESS = ns.weakenAnalyze(1);

    ns.disableLog('asleep');
    ns.clearLog();

    await runScript(ns, SCRIPT_RETRACT);
    [PORT_ROOTKIT, PORT_TENTACLE, PORT_TENTACLE_FEEDBACK].forEach(port => ns.clearPort(port));

    // TODO: kill root script if it's running
    await runScript(ns, SCRIPT_ROOT_KIT);

    // const reservedRam = SCRIPTS_RESERVE.reduce((acc, scriptName) => acc += ns.getScriptRam(scriptName), 0);
    // await runScript(ns, SCRIPT_TENTACLE, Math.floor(reservedRam / ns.getScriptRam(SCRIPT_TENTACLE)));

    let targets = [];
    let skipped = [];
    const allHosts = getAllHosts(ns, HOST_HOME);
    const getThreads = {
        weaken: getThreadsToWeakFull,
        grow: getThreadsToGrowFull,
        hack: getThreadsToHackFull,
    };

    await loop(ns, async () => {
        const message = readPort(ns, PORT_TENTACLE_FEEDBACK);
        if (message) {
            targets.find(({ host }) => host === message).isBusy = false;
        }

        const newTargets = findNewTargets(ns, allHosts, targets);

        if (newTargets.length) {
            targets = [...targets, ...newTargets];
        }

        for (let i = 0; i < targets.length; i++) {
            const { host, isBusy } = targets[i];

            if (isBusy) continue;

            const action = getAction(ns, host);
            const threads = getThreads[action](ns, host);

            if (threads > 9999) {
                if (skipped.findIndex((s) => s.host === host) < 0) {
                    skipped.push({ host, action, threads });
                    await ns.write('skipped.txt', JSON.stringify(skipped), 'w');
                }
                continue;
            }

            if (await runScript(ns, SCRIPT_TENTACLE, threads, host, action)) {
                targets[i].isBusy = true;
            }

            // WTF: what the point to have threads as argument in weaken/grow/hack ( i.e. hack(host, { threads: 100500 }) ) if
            // you can't use several weaken/grow/hack calls in one script?
            /*const isSent = await ns.tryWritePort(PORT_TENTACLE, `${host}:${action}:${threads}`);

            if (isSent) {
                log(ns, SCRIPT_OCTOPUS, `${action}(${host}, ${threads})`, 'info');
                targets[i].isBusy = true;
            }*/
        };
    });
}

// ==================================================================

/** @param {NS} ns **/
async function runScript(ns, scriptName, threads = 1, ...args) {
    const pid = ns.exec(scriptName, HOST_HOME, threads, ...args);

    if (pid < 1) {
        log(ns, SCRIPT_OCTOPUS, `Can't run ${scriptName} with ${threads} threads`, 'error');
        return false;
    }

    let success = true;
    const time = Date.now() + 1000;
    await loop(ns, stop => {
        if (ns.isRunning(pid)) stop();
        if (Date.now() >= time) {
            log(ns, SCRIPT_OCTOPUS, `${scriptName} didn't started`, 'error');
            success = false;
            stop();
        }
    });

    return success;
}

/** @param {NS} ns **/
function readPort(ns, port) {
    const message = ns.readPort(port);

    if (message !== PORT_EMPTY) {
        return message;
    }

    return null;
}

/** @param {NS} ns **/
function findNewTargets(ns, allHosts, targets) {
    const newTargets = withRootAccess(ns, allHosts).filter(host => {
        if (ns.getServerMaxMoney(host) < 1) return false;
        for (let i = 0; i < targets.length; i++) {
            if (targets[i].host === host) return false;
        }

        return true;
    });

    return newTargets.map(host => ({ host, isBusy: false }));
}

/** @param {NS} ns **/
function getAction(ns, host) {
    const securityMin = ns.getServerMinSecurityLevel(host);
    const securityNow = ns.getServerSecurityLevel(host);

    if (securityNow > securityMin) {
        return 'weaken';
    }

    const moneyMax = ns.getServerMaxMoney(host);
    const moneyNow = ns.getServerMoneyAvailable(host);

    if (moneyNow < moneyMax) {
        return 'grow';
    }

    return 'hack';
}

// 1=================================7

/** @param {NS} ns **/
function setBitNode12Mult(ns) {
    const { sourceFiles } = ns.getPlayer();
    if (!sourceFiles?.length) return 1;
    let sf12Lvl = 0;

    for (let i = 0; i < sourceFiles.length; i++) {
        if (sourceFiles[i].n === 12) {
            sf12Lvl = sourceFiles[i].lvl;
        }
    }

    const inc = Math.pow(1.02, sf12Lvl);
    return 1 / inc;
}

/** @param {NS} ns **/
function getBitNodeHackMoneyMult(ns) {
    const { bitNodeN } = ns.getPlayer();
    return BIT_NODE_HACK_MONEY[bitNodeN] ?? 1;
}

/** @param {NS} ns **/
function getBitNodeGrowMult(ns) {
    const { bitNodeN } = ns.getPlayer();
    return BIT_NODE_GROW[bitNodeN] ?? 1;
}

/** @param {NS} ns **/
function getThreadsToHackFull(ns, host) {
    const diffMult = (100 - ns.getServerMinSecurityLevel(host)) / 100;
    const hackSkill = ns.getHackingLevel();
    const { hacking_money_mult } = ns.getPlayer();
    const skillMult = (hackSkill - (ns.getServerRequiredHackingLevel(host) - 1)) / hackSkill;
    const moneyHacked = (diffMult * skillMult * hacking_money_mult * getBitNodeHackMoneyMult(ns)) / BALANCE_FACTOR;
    return Math.ceil(1 / moneyHacked);
}

/** @param {NS} ns **/
function getThreadsToWeakFull(ns, host) {
    return Math.ceil((ns.getServerSecurityLevel(host) - ns.getServerMinSecurityLevel(host)) / SECURITY_PROGRESS);
}

/** @param {NS} ns **/
function getThreadsToGrowFull(ns, host) {
    const growRate = clamp(1 + (GROWTH_RATE - 1) / ns.getServerSecurityLevel(host), 0.1, GROWTH_RATE_MAX);
    const serverGrow = ns.getServerGrowth(host) / 100 * getBitNodeGrowMult(ns);
    const { hacking_grow_mult } = ns.getPlayer();
    const mult = Math.pow(growRate, serverGrow * hacking_grow_mult);

    const moneyMax = ns.getServerMaxMoney(host);
    const money = ns.getServerMoneyAvailable(host) + 1;
    return Math.ceil(logBase(moneyMax / money, mult));
}

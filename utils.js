/** @param {NS} ns **/
export function log(ns, prefix, message, level = 'info') {
    ns.toast(`${prefix}: ${message}`, level, 10000);
    ns.print(message);
}

/** @param {NS} ns **/
export async function loop(ns, fn, interval = 25) {
    let running = true;
    const stop = () => running = false;

    while (running) {
        await fn(stop);
        await ns.asleep(interval);
    }
}

/** @param {NS} ns **/
export function getAllHosts(ns, startHost) {
    const collect = (host, prev) => {
        const around = ns.scan(host).filter(h => h !== prev);
        return [...around, ...around.map(h => collect(h, host))].flat();
    };

    return collect(startHost);
}

/** @param {NS} ns **/
export function withRootAccess(ns, hosts) {
    return hosts.filter(host => ns.hasRootAccess(host));
}

/** @param {NS} ns **/
export function withoutRootAccess(ns, hosts) {
    return hosts.filter(host => !ns.hasRootAccess(host));
}

export function clamp(value, lo = 0.0, hi = 1.0) {
    return (value < lo) ? lo : (value > hi) ? hi : value;
}

export function logBase(n, base) {
    return base > 0 ? Math.log(n) / Math.log(base) : Math.log(n);
}

export function splitArray(arr) {
    const len = arr.length;

    if (len <= 1) return arr;

    const half = len * 0.5;
    return [arr.slice(0, half), arr.slice(half)];
}

export function notScript(filename) {
    return !filename.endsWith('.js') && !filename.endsWith('.ns') && !filename.endsWith('.script');
}

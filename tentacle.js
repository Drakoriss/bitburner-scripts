// const PORT_TENTACLE = 2;
const PORT_TENTACLE_FEEDBACK = 3;
// const EMPTY_PORT = 'NULL PORT DATA';

// WTF: what the point to have threads as argument in weaken/grow/hack ( i.e. hack(host, { threads: 100500 }) ) if
// you can't use several weaken/grow/hack calls in one script?
/** @param {NS} ns **/
/*export async function main(ns) {
  ns.disableLog('asleep');
  ns.clearLog();

  const threadsMax = ns.args[0];

  const actions = {
    weaken: ns.weaken,
    grow: ns.grow,
    hack: ns.hack,
  };


  while (true) {
    const message = ns.readPort(PORT_TENTACLE);

    if (message !== EMPTY_PORT) {
      const [host, action, threads] = message.split(':');
      new Promise(async done => {
        await actions[action](host, { threads, stock: false });
        await ns.tryWritePort(PORT_TENTACLE_FEEDBACK, host);
        done();
      });
    } else {
      await ns.asleep(25);
    }
  }
}*/

/** @param {NS} ns **/
export async function main(ns) {
  const [host, action] = ns.args;

  const actions = {
    weaken: ns.weaken,
    grow: ns.grow,
    hack: ns.hack,
  };

  await actions[action](host, { stock: false });
  await ns.tryWritePort(PORT_TENTACLE_FEEDBACK, host);
}

# Birburner Scripts

## Description

A set of homebrew scripts to automate some stuff that is kept in one place and can be downloaded automatically.

## How to use

Just download a `scripts-fetcher.js` to your home host through `wget`:
```bash
wget https://gitlab.com/Drakoriss/bitburner-scripts/-/raw/main/scripts-fetcher.js scripts-fetcher.js
```

Then run it:
```bash
run scripts-fetcher.js
```

You can remove the script after it successfully downloads all necessary scripts.

## Scripts

### scripts-fetcher.js

Used only to get other scripts to your home host.

### utils.js

A set of utility functions that are used among other scripts. Not designed to run directly.

### common.js

Contains constants that are used among other scripts. Not designed to run directly.

### lore-kit.js

Searches all the hosts to copy lore files (.lit, .txt, .msg) to the home host and to find all the contracts files. Contract files will be written to the `contracts.txt` on the home host in a form:
```typescript
{ host: string, file: string }[]
```
So you can `JSON.parse` the contents of this file. Also, you can collect contracts only without copying lore files by passing `true` as an argument:
```bash
run lore-kit.js true
```

### hacknet-kit.js

Buys and upgrades hacknet nodes until the script is stopped manually. Be warned that script drains money fast.

### retract-tentacles.js

Kills a `tentacle.js` scripts amongst all hosts. `octopus.js` uses this script.

### root-kit.js

Searches all the hosts to find ones that can be rooted and roots them. `octopus.js` starts this script.
Ports in use: 1

### tentacle.js

Simple script that can `hack` / `grow` / `weaken`. `octopus.js` uses this script.
Ports in use: 3
Arguments:
1. hostname: string
2. action: 'weaken' || 'grow' || 'hack'

### octopus.js

Script that spawns `tentacle.js` on home host to hack the money. Also launches `root-kit.js`.
Ports in use: 1, 2, 3

### gang-kit.js

Script for bitNode=2 that automates handling of a gang. You must join the gang before using this script.
Also note that currently this script is for combat gangs.

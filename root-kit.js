import { HOST_HOME, TOOLS_ROOT, SCRIPT_ROOT_KIT, SCRIPT_TENTACLE, PORT_ROOTKIT } from './common.js';
import { getAllHosts, withoutRootAccess, log, loop } from './utils.js';

/** @param {NS} ns **/
export async function main(ns) {
    ns.disableLog('asleep');

    const hosts = getAllHosts(ns, HOST_HOME);

    await loop(ns, async stop => {
        const notRootedHosts = withoutRootAccess(ns, hosts);
        const hackSkill = ns.getHackingLevel();
        const rootable = notRootedHosts.filter(host => ns.getServerRequiredHackingLevel(host) <= hackSkill);
        const methods = getRootMethods(ns);
        let rooted = 0;
        // let threadsTotal = 0;

        for (let i = 0; i < rootable.length; i++) {
            const host = rootable[i];
            const ports = ns.getServerNumPortsRequired(host);

            if (ports <= methods.length) {
                methods.forEach(method => method(host));

                ns.nuke(host);
                rooted += 1;
                // TODO: case when home server don't have enough RAM to support all the targets
                // Because home server can upgrade cores (weaken and grow speed) you want all scripts to work on your home server. But
                // from start you have just 1 core and not enough RAM so scripts must work outside home server too.
                /*await ns.scp(SCRIPT_TENTACLE, HOST_HOME, host);
                const scriptRam = ns.getScriptRam(SCRIPT_TENTACLE, host);
                const serverRam = ns.getServerMaxRam(host);

                if (serverRam >= scriptRam) {
                    const threads = Math.floor(serverRam / scriptRam);
                    const pid = ns.exec(SCRIPT_TENTACLE, host, threads, threads);

                    if (pid < 1) {
                        log(ns, SCRIPT_ROOT_KIT, `can't run ${SCRIPT_TENTACLE} on ${host}`, 'error');
                        return stop();
                    }

                    rooted += 1;
                    threadsTotal += threads;
                }*/
            }
        }

        if (rooted > 0) {
            log(ns, SCRIPT_ROOT_KIT, `rooted ${rooted} hosts`, 'success');
            // log(ns, SCRIPT_ROOT_KIT, `rooted ${rooted} hosts with total ${threadsTotal} threads`, 'success');
            await ns.tryWritePort(PORT_ROOTKIT, rooted);
        }
    });
}

/** @param {NS} ns **/
function getRootMethods(ns) {
    const methods = [ns.brutessh, ns.ftpcrack, ns.relaysmtp, ns.httpworm, ns.sqlinject];
    const result = [];
    TOOLS_ROOT.forEach((tool, i) => ns.fileExists(tool, HOST_HOME) && result.push(methods[i]));
    return result;
}

export const PORT_EMPTY = 'NULL PORT DATA';
export const PORT_ROOTKIT = 1;
export const PORT_TENTACLE = 2;
export const PORT_TENTACLE_FEEDBACK = 3;

export const HOST_HOME = 'home';

export const SCRIPT_OCTOPUS = 'octopus.js';
export const SCRIPT_ROOT_KIT = 'root-kit.js';
export const SCRIPT_TENTACLE = 'tentacle.js';
export const SCRIPT_RETRACT = 'retract-tentacles.js';
export const SCRIPT_LORE_KIT = 'lore-kit.js';
export const SCRIPT_HACKNET_KIT = 'hacknet-kit.js';
export const SCRIPTS_RESERVE = [SCRIPT_OCTOPUS, SCRIPT_ROOT_KIT, SCRIPT_RETRACT, SCRIPT_LORE_KIT, SCRIPT_HACKNET_KIT];

export const TOOLS_ROOT = ['BruteSSH.exe', 'FTPCrack.exe', 'relaySMTP.exe', 'HTTPWorm.exe', 'SQLInject.exe'];

export const FILE_CONTRACTS = 'contracts.txt';

/** @param {NS} ns **/
export async function main(ns) {
    const scripts = [
        'utils.js',
        'common.js',
        'root-kit.js',
        'lore-kit.js',
        'hacknet-kit.js',
        'tentacle.js',
        'retract-tentacles.js',
        'octopus.js',
        'gang-kit.js',
    ];
    const homeHost = 'home';
    const repo = 'https://gitlab.com/Drakoriss/bitburner-scripts/-/raw/main';

    for (let i = 0; i < scripts.length; i++) {
        const name = scripts[i];
        const result = await ns.wget(`${repo}/${name}`, name, homeHost);

        if (!result) {
            ns.toast(`Failed to download "${name}"`, 'error', 5000);
            ns.exit();
        }
    }

    ns.toast('All scripts fetched successfully', 'success', 5000);
    // Seems you can't remove a running script, even inside ns.atExit
    ns.rm('scripts-fetcher.js', homeHost);
}

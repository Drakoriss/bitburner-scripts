import { getAllHosts, withoutRootAccess, log } from './utils.js';
import { HOME_HOST, ROOT_TOOLS, ROOTKIT } from './common.js';

/** @param {NS} ns **/
export async function main(ns) {
    const hosts = getAllHosts(ns, HOME_HOST);
    const notRootedHosts = withoutRootAccess(ns, hosts);
    const hackSkill = ns.getHackingLevel();
    const rootable = notRootedHosts.filter(host => ns.getServerRequiredHackingLevel(host) <= hackSkill);
    const methods = getRootMethods(ns);
    let rooted = 0;

    rootable.forEach(host => {
        const ports = ns.getServerNumPortsRequired(host);

        if (ports <= methods.length) {

            for (let i = 0; i < ports; i++) {
                methods[i](host);
            }

            ns.nuke(host);
            rooted += 1;
        }
    });

    if (rooted > 0) {
        log(ns, ROOTKIT, `rooted ${rooted} hosts`, 'success');
    }
}

/** @param {NS} ns **/
function getRootMethods(ns) {
    const methods = [ns.brutessh, ns.ftpcrack, ns.relaysmtp, ns.httpworm, ns.sqlinject];
    const result = [];
    ROOT_TOOLS.forEach((tool, i) => ns.fileExists(tool, HOME_HOST) && result.push(methods[i]));
    return result;
}

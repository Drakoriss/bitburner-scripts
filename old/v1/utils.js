import { HOME_HOST, KITS } from './common.js';

/** @param {NS} ns **/
export function log(ns, prefix, message, level = 'info') {
    ns.toast(`${prefix}: ${message}`, level, 5000);
    ns.print(message);
}

/** @param {NS} ns **/
export async function loop(ns, fn, interval = 50) {
    let running = true;
    const stop = () => running = false;

    while (running) {
        await fn(stop);
        await ns.asleep(interval);
    }
}

/** @param {NS} ns **/
export function getAllHosts(ns, startHost) {
    const collect = (host, prev) => {
        const around = ns.scan(host).filter(h => h !== prev);
        return [...around, ...around.map(h => collect(h, host))].flat();
    };

    return collect(startHost);
}

/** @param {NS} ns **/
export function withRootAccess(ns, hosts) {
    return hosts.filter(host => ns.hasRootAccess(host));
}

/** @param {NS} ns **/
export function withoutRootAccess(ns, hosts) {
    return hosts.filter(host => !ns.hasRootAccess(host));
}

/** @param {NS} ns **/
export function threadsLeftForScript(ns, host, script) {
    const scriptRam = ns.getScriptRam(script);

    if (host === HOME_HOST) {
        return Math.floor(getHomeHostFreeMem(ns) / scriptRam);
    }

    const maxRam = ns.getServerMaxRam(host);

    if (maxRam < scriptRam) return 0;

    return Math.floor((ns.getServerMaxRam(host) - ns.getServerUsedRam(host)) / scriptRam);
}

export function notScript(filename) {
    return !filename.endsWith('.js') && !filename.endsWith('.ns') && !filename.endsWith('.script');
}

/** @param {NS} ns **/
export function getHomeHostFreeMem(ns) {
    const reservedRam = KITS.reduce((acc, script) => acc += ns.getScriptRam(script, HOME_HOST), 0);
    return ns.getServerMaxRam(HOME_HOST) - ns.getServerUsedRam(HOME_HOST) - reservedRam;
}

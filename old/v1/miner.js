/** @param {NS} ns **/
export async function main(ns) {
  const target = ns.args[0];
  const moneyCap = ns.getServerMaxMoney(target) * 0.85;
  const securityCap = ns.getServerMinSecurityLevel(target) + 5;

  while (true) {
    if (ns.getServerSecurityLevel(target) > securityCap) {
      await ns.weaken(target);
    } else if (ns.getServerMoneyAvailable(target) < moneyCap) {
      await ns.grow(target);
    } else {
      await ns.hack(target);
    }
  }
}

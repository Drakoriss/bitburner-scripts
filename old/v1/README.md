# Birburner Scripts

## Description

A set of homebrew scripts to automate some stuff that is kept in one place and can be downloaded automatically.

## How to use

Just download a `scripts-fetcher.js` to your home host through `wget`:
```bash
wget https://gitlab.com/Drakoriss/bitburner-scripts/-/raw/main/scripts-fetcher.js scripts-fetcher.js
```

Then run it:
```bash
run scripts-fetcher.js
```

You can remove the script after it successfully downloads all necessary scripts.

## Scripts

### scripts-fetcher.js

Used only to get other scripts to your home host.

### utils.js

A set of utility functions that are used among other scripts. Not designed to run directly.

### common.js

Contains constants that are used among other scripts. Not designed to run directly.

### root-kit.js

Searches all the hosts to find ones that can be rooted and roots them. `overmind.js` uses this script.

### lore-kit.js

Searches all the hosts to copy lore files (.lit, .txt, .msg) to the home host and to find all the contracts files. Contract files will be written to the `contracts.txt` on the home host in a form:
```typescript
{ host: string, file: string }[]
```
So you can `JSON.parse` the contents of this file. Also, you can collect contracts only without copying lore files by passing `true` as an argument:
```bash
run lore-kit.js true
```

### hacknet-kit.js

Buys and upgrades hacknet nodes until the script is stopped manually. Be warned that script drains money fast.

### miner.js

Simple script that can `hack` / `grow` / `weaken` depending on a host stats. `hack-kit.js` uses this script.
Arguments:
1. hostname: string

### stop-miners.js

Kills a miner scripts amongst all hosts. `overmind.js` uses this script.

### hack-kit.js

Runs a miner (copied if necessary) on a specified host with a provided number of threads. Threads are shared among home host, purchased servers, and hosts that you have root access to. `overmind.js` uses this script.
Arguments:
1. hostname: string
2. threads: number

### overmind.js

Orchestrates the process of automatic rooting and hacking all available hosts.

### server-kit.js

Purchases a new server with the most ram possible. Knows how to replace servers with low ram if max server limit is reached.

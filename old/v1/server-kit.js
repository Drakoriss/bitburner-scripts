import { SERVERKIT } from './common.js';
import { log } from './utils.js';

/** @param {NS} ns **/
export async function main(ns) {
    const ramCost = [];

    for (let i = 1; i <= 20; i++) {
        const ram = 2 ** i;
        const cost = ns.getPurchasedServerCost(ram);
        ramCost.push({ cost, ram });
    }

    ramCost.reverse();

    const { money } = ns.getPlayer();
    const affordable = ramCost.find(({ cost }) => cost <= money);

    if (!affordable) return;

    const owned = ns.getPurchasedServers()
        .map(host => ({ host, ram: ns.getServerMaxRam(host) }))
        .sort(byRamAsc);

    if (owned.length >= ns.getPurchasedServerLimit()) {
        const lowestRam = owned.find(({ ram }) => ram < affordable.ram);

        if (lowestRam) {
            ns.killall(lowestRam.host);
            ns.deleteServer(lowestRam.host);
        } else {
            return;
        }
    }

    const name = ns.purchaseServer(generateName(), affordable.ram);

    if (name.length) {
        log(ns, SERVERKIT, `${affordable.ram}GB server purchased`, 'success');
    }
}

function byRamAsc(a, b) {
    if (a.ram < b.ram) return -1;
    if (a.ram > b.ram) return 1;
    return 0;
}

function generateName() {
    return `miner-${Math.random().toString(16).substring(2)}`;
}

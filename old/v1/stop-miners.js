import { HOME_HOST, MINER, STOPMINERS } from './common.js';
import { getAllHosts, withRootAccess, log } from './utils.js';

/** @param {NS} ns **/
export async function main(ns) {
    const allHosts = getAllHosts(ns, HOME_HOST);
    const rootedHosts = withRootAccess(ns, allHosts);
    const ownedHosts = ns.getPurchasedServers();
    const hosts = [HOME_HOST, ...ownedHosts, ...rootedHosts];
    let killed = 0;

    hosts.forEach(host => {
        if (ns.scriptRunning(MINER, host)) {
            ns.scriptKill(MINER, host);
            killed += 1;
        }
    });

    log(ns, STOPMINERS, `killed ${killed} miners`);
}

import { HOME_HOST, MINER, HACKKIT, OVERMIND_PORT } from './common.js';
import { getAllHosts, withRootAccess, threadsLeftForScript, log } from './utils.js';

/** @param {NS} ns **/
export async function main(ns) {
    const target = ns.args[0];
    const totalThreads = ns.args[1];
    let availableThreads = totalThreads;
    const allHosts = getAllHosts(ns, HOME_HOST);
    const rootedHosts = withRootAccess(ns, allHosts);
    const ownedHosts = ns.getPurchasedServers();
    const hosts = [HOME_HOST, ...ownedHosts, ...rootedHosts];

    for (let i = 0; i < hosts.length; i++) {
        if (availableThreads < 1) break;

        const host = hosts[i];
        const freeThreads = threadsLeftForScript(ns, host, MINER);

        if (freeThreads < 1) continue;

        await copyMiner(ns, host);

        if (freeThreads <= availableThreads) {
            await runMiner(ns, host, freeThreads, target);
            availableThreads -= freeThreads;
        } else if (availableThreads < freeThreads) {
            await runMiner(ns, host, availableThreads, target);
            availableThreads = 0;
            break;
        }
    }

    const used = totalThreads - availableThreads;

    if (used > 0) {
        log(ns, HACKKIT, `used ${used} threads to mine "${target}"`, 'success');
    }

    await ns.tryWritePort(OVERMIND_PORT, used);
}

/** @param {NS} ns **/
async function copyMiner(ns, host) {
    if (!ns.fileExists(MINER, host)) {
        await ns.scp(MINER, HOME_HOST, host);
    }
}

/** @param {NS} ns **/
async function runMiner(ns, host, threads, targetHost) {
    if (threads < 1) return;
    const pid = ns.exec(MINER, host, threads, targetHost);

    if (pid < 1) {
        log(ns, HACKKIT, `can't run miner(${targetHost}) on ${host}`, 'error');
        await ns.tryWritePort(OVERMIND_PORT, 'ERROR');
        ns.exit();
    }
}

import { HOME_HOST, MINER, STOPMINERS, ROOTKIT, HACKKIT, OVERMIND_PORT, PORT_EMPTY, HOSTS } from './common.js';
import { getAllHosts, loop } from './utils.js';

let SCRIPT_RAM;

/** @param {NS} ns **/
export async function main(ns) {
    ns.disableLog('asleep');
    ns.clearPort(OVERMIND_PORT);
    ns.clearLog();

    ns.exec(STOPMINERS, HOME_HOST);
    SCRIPT_RAM = ns.getScriptRam(MINER, HOME_HOST);

    const ownedServers = ns.getPurchasedServers();
    let targets = mapStats(
        ns,
        getAllHosts(ns, HOME_HOST).filter(host => !ownedServers.includes(host)),
    ).sort(byStats);

    await loop(ns, async () => {
        const target = targets.find(({ maxThreads, threadsUsed, rooted }) => rooted && (threadsUsed < maxThreads));

        if (target) {
            ns.exec(HACKKIT, HOME_HOST, 1, target.host, target.maxThreads - target.threadsUsed);

            const time = Date.now() + 2000;
            await loop(ns, stop => {
                const threadsUsed = ns.readPort(OVERMIND_PORT);

                if (threadsUsed !== PORT_EMPTY) {
                    if (threadsUsed === 'ERROR') return stop();

                    target.threadsUsed += threadsUsed;
                    return stop();
                }

                if (time <= Date.now()) {
                    stop();
                }
            });
        }

        ns.exec(ROOTKIT, HOME_HOST);
        targets = updateStats(ns, targets);
        targets.forEach(target => target.rooted = ns.hasRootAccess(target.host));
        // Just to check if threads are used
        await ns.write(HOSTS, JSON.stringify(targets), 'w');
    }, 1000);
}

/** @param {NS} ns **/
function mapStats(ns, hosts) {
    return hosts.map(host => {
        const money = ns.hackAnalyze(host);
        const suitable = SCRIPT_RAM <= ns.getServerMaxRam(host) && money > 0;

        return suitable ? {
            host,
            hackTime: ns.getHackTime(host),
            hackMoney: money,
            hackChance: ns.hackAnalyzeChance(host),
            maxThreads: Math.floor(1 / money),
            threadsUsed: 0,
            rooted: ns.hasRootAccess(host),
        } : null;
    }).filter(v => v);
}

function byStats(a, b) {
    if (a.hackTime < b.hackTime && a.hackMoney > b.hackMoney && a.hackChance > b.hackChance) {
        return -1;
    }

    if (a.hackTime > b.hackTime && a.hackMoney < b.hackMoney && a.hackChance < b.hackChance) {
        return 1;
    }

    return 0;
}

/** @param {NS} ns **/
function updateStats(ns, targets) {
    const ownedServers = ns.getPurchasedServers();
    const oldTargets = targets.map(({ host }) => host);
    const hosts = getAllHosts(ns, HOME_HOST).filter(host => !ownedServers.includes(host) && !oldTargets.includes(host));

    if (hosts.length) {
        return [...targets, ...mapStats(ns, hosts)].sort(byStats);
    }

    return targets;
}

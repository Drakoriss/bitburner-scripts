import { HOST_HOME, SCRIPT_TENTACLE, SCRIPT_RETRACT } from './common.js';
import { getAllHosts, withRootAccess, log } from './utils.js';

/** @param {NS} ns **/
export async function main(ns) {
    const allHosts = getAllHosts(ns, HOST_HOME);
    const rootedHosts = withRootAccess(ns, allHosts);
    const hosts = [HOST_HOME, ...rootedHosts];
    let killed = 0;

    hosts.forEach(host => {
        if (ns.scriptRunning(SCRIPT_TENTACLE, host)) {
            ns.scriptKill(SCRIPT_TENTACLE, host);
            killed += 1;
        }
    });

    if (killed > 0) {
        log(ns, SCRIPT_RETRACT, `retracted ${killed} tentacles`);
    }
}

import { getAllHosts, log, notScript } from './utils.js';
import { HOST_HOME, SCRIPT_LORE_KIT, FILE_CONTRACTS } from './common.js';

/** @param {NS} ns **/
export async function main(ns) {
    const ignoreLore = ns.args[0];
    const hosts = getAllHosts(ns, HOST_HOME);
    const contracts = [];
    let copied = 0;

    for (let i = 0; i < hosts.length; i++) {
        const host = hosts[i];
        const files = ns.ls(host).filter(notScript);
        const lore = [];

        for (let j = 0; j < files.length; j++) {
            const file = files[j];

            if (file.endsWith('.cct')) {
                contracts.push({ host, file });
            } else if (!ignoreLore) {
                lore.push(file);
            }
        }

        if (lore.length) {
            await ns.scp(lore, host, HOST_HOME);
            copied += lore.length;
        }
    }

    if (contracts.length) {
        await ns.write(FILE_CONTRACTS, JSON.stringify(contracts), 'w');
        log(ns, SCRIPT_LORE_KIT, `found ${contracts.length} contracts`, 'success');
    }

    if (copied > 0) {
        log(ns, SCRIPT_LORE_KIT, `copied ${copied} files`, 'success');
    }
}

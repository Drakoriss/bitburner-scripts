import { loop } from './utils.js';

const BIT_NODE_GANG = {
    3: 0.9,
    6: 0.7,
    7: 0.7,
    8: 0,
    9: 0.8,
    10: 0.9,
    12: 0.8,
    13: 0.3,
};

/** @param {NS} ns **/
function getBitNodeGangMult(ns) {
    const { bitNodeN } = ns.getPlayer();
    return BIT_NODE_GANG[bitNodeN] || 1;
}

const TASK_IDLE = 'Unassigned';
const TASK_TRAIN_HACK = 'Train Hacking';
const TASK_TRAIN_COMBAT = 'Train Combat';
const TASK_TRAIN_CHA = 'Train Charisma';
const TASK_TERRITORY = 'Territory Warfare';

// const SKILLS = ['hack', 'str', 'def', 'dex', 'agi', 'cha'];
const SKILLS = ['str', 'def', 'dex', 'agi'];
const TASK_TRAIN = {
    hack: TASK_TRAIN_HACK,
    cha: TASK_TRAIN_CHA,
};

function getTrainTask(skill) {
    return TASK_TRAIN[skill] || TASK_TRAIN_COMBAT;
}

let GANGS;

/** @param {NS} ns **/
export async function main(ns) {
    ns.disableLog('asleep');
    ns.clearLog();

    const ownName = ns.gang.getGangInformation().faction;
    GANGS = Object.keys(ns.gang.getOtherGangInformation()).filter(name => name !== ownName);

    await loop(ns, () => {
        if (ns.gang.canRecruitMember()) {
            ns.gang.recruitMember(generateName());
        }

        const entities = ns.gang.getMemberNames().map(entity => ns.gang.getMemberInformation(entity));
        const tasks = ns.gang.getTaskNames().map(task => ns.gang.getTaskStats(task));
        const gang = ns.gang.getGangInformation();
        const equip = ns.gang.getEquipmentNames()
            .filter(name => onlyCombatStuff(ns.gang.getEquipmentStats(name)))
            .filter(name => ns.gang.getEquipmentType(name) !== 'Augmentation')
            .map(name => ({ name, cost: ns.gang.getEquipmentCost(name) })).sort(byCostAsc);
        let onWarfare = 0;

        entities.forEach(entity => {
            if (equipCourse(ns, entity, gang, equip, tasks)) return;

            if (ascensionCourse(ns, entity)) return;

            // if (trainCourse(ns, entity)) return;

            // if (wantedDecreaseCourse(ns, entity, gang, tasks)) return;

            if (territoryCourse(ns, entity, gang)) return onWarfare += 1;

            // if (respectIncreaseCourse(ns, entity, gang, tasks)) return;

            moneyCourse(ns, entity, gang, tasks);
        });

        ns.gang.setTerritoryWarfare(entities.length === onWarfare && canBeatAll(ns));
    }, 2000);
}

function generateName() {
    return `entity-${Math.random().toString(16).substring(2)}`;
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {string} taskName
 */
function assignTask(ns, entity, taskName) {
    if (entity.task !== taskName) ns.gang.setMemberTask(entity.name, taskName);
    return true;
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @returns {boolean} true if task was assigned
 */
function trainCourse(ns, entity) {
    const skill = findLesserSkill(entity);

    if (skill.value > 10000) return false;

    return assignTask(ns, entity, getTrainTask(skill.name))
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @returns {boolean} true if task was assigned
 */
function ascensionCourse(ns, entity) {
    const skill = findLesserSkill(entity);

    if (skill.ascValue > 10) return false;

    const ascension = ns.gang.getAscensionResult(entity.name);

    if (ascension && skill.ascValue * (ascension[skill.name] - 1) > 1) {
        ns.gang.ascendMember(entity.name);
        return false;
    }

    return assignTask(ns, entity, getTrainTask(skill.name));
}

/** @param {GangMemberInfo} entity **/
function findLesserSkill(entity) {
    return SKILLS.reduce((lesser, name) => entity[`${name}_asc_mult`] < lesser.ascValue ? { name, ascValue: entity[`${name}_asc_mult`], value: entity[name] } : lesser, { name: null, ascValue: Infinity, value: 1 });
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {GangGenInfo} gang
 * @param {GangTaskStats[]} tasks
 * @returns {boolean} true if task was assigned
 */
function wantedDecreaseCourse(ns, entity, gang, tasks) {
    if (gang.wantedLevel > 1) {
        const mostEffectiveTask = tasks.reduce((chosen, current) => current.baseWanted < chosen.baseWanted ? current : chosen, tasks[0]);
        return assignTask(ns, entity, mostEffectiveTask.name);
    }

    return false;
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {GangGenInfo} gang
 * @param {GangTaskStats[]} tasks
 * @returns {boolean} true if task was assigned
 */
function respectIncreaseCourse(ns, entity, gang, tasks) {
    if (gang.respect > 1000000) return false;

    const mostEffectiveTask = tasks.reduce((chosen, current) => current.baseRespect > chosen.baseRespect ? chosen = current : chosen, tasks[0]);
    return assignTask(ns, entity, mostEffectiveTask.name);
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {GangGenInfo} gang
 * @param {{ name: string, cost: number }} equip
 * @param {GangTaskStats[]} tasks
 * @returns {boolean} true if task was assigned
 */
function equipCourse(ns, entity, gang, equip, tasks) {
    const availableEquip = equip.filter(({ name }) => !entity.upgrades.includes(name) && !entity.augmentations.includes(name));

    if (availableEquip.length < 1) return false;

    if (availableEquip[0].cost <= ns.getServerMoneyAvailable('home')) {
        ns.gang.purchaseEquipment(entity.name, availableEquip[0].name);
        return false;
    }

    if (wantedDecreaseCourse(ns, entity, gang, tasks)) return true;

    const taskName = findMostProfitableTask(ns, entity, gang, tasks);

    if (!taskName) return false;

    return assignTask(ns, entity, taskName);
}

function byCostAsc(a, b) {
    if (a.cost < b.cost) return -1;
    if (a.cost > b.cost) return 1;
    return 0;
}

function byMoneyDesc(a, b) {
    if (a.money < b.money) return 1;
    if (a.money > b.money) return -1;
    return 0;
}

/**
 * @param {GangMemberInfo} entity
 * @param {GangTaskStats} task
 */
function getStatWeight(entity, task) {
    return SKILLS.reduce((weight, skill) => weight += entity[skill] * (task[`${skill}Weight`] / 100), 0);
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {GangGenInfo} gang
 * @param {GangTaskStats} task
 */
function getTaskMoney(ns, entity, gang, task) {
    const weight = getStatWeight(entity, task) - 3.2 * task.difficulty;

    if (weight <= 0) return 0;

    const territoryMult = Math.max(0.005, Math.pow(gang.territory * 100, task.territory.money) / 100);
    if (isNaN(territoryMult) || territoryMult <= 0) return 0;
    const respectMult = gang.respect / (gang.respect + gang.wantedLevel);
    const territoryPenalty = (0.2 * gang.territory + 0.8) * getBitNodeGangMult(ns);
    return Math.pow(5 * task.baseMoney * weight * territoryMult * respectMult, territoryPenalty);
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {GangGenInfo} gang
 * @param {GangTaskStats[]} tasks
 */
function findMostProfitableTask(ns, entity, gang, tasks) {
    const top = tasks
        .filter(({ baseMoney }) => baseMoney > 0)
        .map(task => ({ name: task.name, money: getTaskMoney(ns, entity, gang, task) }))
        .sort(byMoneyDesc);

    return top[0].money > 0 ? top[0].name : null;
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {GangGenInfo} gang
 * @param {GangTaskStats[]} tasks
 * @returns {boolean} true if task was assigned
 */
function territoryCourse(ns, entity, gang) {
    if (gang.territory > 0.99) return false;

    return assignTask(ns, entity, TASK_TERRITORY);
}

/**
 * @param {NS} ns
 * @param {GangMemberInfo} entity
 * @param {GangGenInfo} gang
 * @param {GangTaskStats[]} tasks
 * @returns {boolean} true if task was assigned
 */
function moneyCourse(ns, entity, gang, tasks) {
    const taskName = findMostProfitableTask(ns, entity, gang, tasks);
    return assignTask(ns, entity, taskName);
}

function onlyCombatStuff(stats) {
    return Object.keys(stats).filter(name => SKILLS.includes(name)).length > 0;
}

/** @param {NS} ns **/
function canBeatAll(ns) {
    const chances = GANGS.map(name => ns.gang.getChanceToWinClash(name)).filter(chance => chance > 0.98);
    return chances.length === GANGS.length;
}

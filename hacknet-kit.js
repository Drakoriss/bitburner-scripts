import { loop } from './utils.js';

const COST = [
    'getLevelUpgradeCost',
    'getRamUpgradeCost',
    'getCoreUpgradeCost',
];

const UPGRADE = [
    'upgradeLevel',
    'upgradeRam',
    'upgradeCore',
];

/** @param {NS} ns **/
export async function main(ns) {
    ns.disableLog('asleep');

    await loop(ns, async () => {
        const { money } = ns.getPlayer();

        if (ns.hacknet.numNodes() < 1) {
            if (ns.hacknet.getPurchaseNodeCost() < money) {
                ns.hacknet.purchaseNode();
            }

            return;
        }

        const result = findLowCostIndex(ns);

        if (result.newNode) {
            if (ns.hacknet.numNodes() < ns.hacknet.maxNumNodes() && ns.hacknet.getPurchaseNodeCost() < money) {
                ns.hacknet.purchaseNode();
            }
        } else {
            if (ns.hacknet[COST[result.param]](result.node, 1) < money) {
                ns.hacknet[UPGRADE[result.param]](result.node, 1);
            }
        }
    });
}

/** @param {NS} ns **/
function findLowCostIndex(ns) {
    const amount = ns.hacknet.numNodes();
    let nodeIndex;
    let costIndex;
    let least = Infinity;

    for (let node = 0; node < amount; node++) {
        COST.forEach((method, costPos) => {
            const value = ns.hacknet[method](node, 1);

            if (value < least) {
                nodeIndex = node;
                costIndex = costPos;
                least = value;
            }
        });
    }

    if (isNaN(parseInt(costIndex)) || isNaN(parseInt(nodeIndex))) {
        return { newNode: true };
    }

    const newNode = ns.hacknet.getPurchaseNodeCost() < ns.hacknet[COST[costIndex]](nodeIndex, 1);

    return { newNode, node: nodeIndex, param: costIndex };
}
